import sys
from os.path import expanduser
import os
from shutil import copyfile
from PyQt5.QtWidgets import QSlider,QVBoxLayout,QHBoxLayout,QApplication, QStyleFactory,QWidget,QComboBox, QLabel, QPushButton,QFileDialog,QDialog,QRadioButton,QCheckBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot,QUrl,Qt
import vlc
from vidgear.gears import CamGear
from vidgear.gears import WriteGear
import cv2
import numpy as np


# path = "C:\\Users\\vmxit\\Desktop\\pupu\\vids\\3126.mp4"

windowTitle = "StockVideos Video Processor"




class Main:
	filesList = []
	sharpness = 1
	horizontalFlip = "yes"
	selectedDirectory = ""
	ui_theme = 'Breeze'
	homeDirectory = expanduser( "~" )
	def process_video( self , path ):
		stream = CamGear(source=path).start()
		writer = WriteGear(output_filename=path+"_processed.mp4")
		i=0
		print(self.horizontalFlip)
		while True:
		   frame = stream.read()
		   if self.horizontalFlip == "yes":
		   	frame = cv2.flip( frame , 1 )
		    # i=i+1
		   if frame is None:
		   	break
		   if self.sharpness == 1:
		   	filter = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
		   	frame=cv2.filter2D(frame,-1,filter)
		   if self.sharpness == 2:
		   	filter = np.array([[-1,-1,-1], 
                              [-1, 8,-1],
                              [-1,-1,-1]])
		   	frame=cv2.filter2D(frame,-1,filter)
		   if self.sharpness == 3:
		   	filter = np.array([[-1,-1,-1], 
                              [-1, 9,-1],
                              [-1,-1,-1]])
		   	frame=cv2.filter2D(frame,-1,filter)
		   if self.sharpness == 4:
		   	filter = np.array([[-1,-1,-1], 
                              [-1, 10,-1],
                              [-1,-1,-1]])
		   	frame=cv2.filter2D(frame,-1,filter)
		   if self.sharpness == 5:
		   	filter = np.array([[-1,-1,-1], 
                              [-1, 11,-1],
                              [-1,-1,-1]])
		   	frame=cv2.filter2D(frame,-1,filter)
		   # hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
		   # greenMask = cv2.inRange(hsv, (26, 10, 30), (97, 100, 255))
		   # frame = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
		   # imghsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV).astype("float32")
		   # filter = ImageEnhance.Color(frame)
			# frame.filter(0.5)
		   cv2.imshow( "Playing" , frame )
		   # writer.write(frame)
		    # if i % 2 == 0:
		    # 	frame = cv2.flip( frame , 1 )
		    # 	filter = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
		    # 	frame=cv2.filter2D(frame,-1,filter)
		    # 	cv2.imshow("Output Frame", frame)
		   writer.write(frame)
		   key = cv2.waitKey(30) & 0xFF
		   if key == ord("q"):
		   	break
		cv2.destroyAllWindows()
		stream.stop()
		writer.close()
	def show_contents( self ):
		self.filesList = os.listdir( self.selectedDirectory )
		# print( self.filesList )
		# first = self.selectedDirectory + "/" + self.filesList[ 0 ]
		# copyfile(first, first+"sa")
		# first = os.path.normpath( first)
		# print( first )
		for path in self.filesList:
			path = os.path.join( self.selectedDirectory , path )
			print(path)
			path = os.path.normpath( path )
			self.process_video( path )
		# media = vlc.MediaTrack( first )
		# la = media.tracks_get()
		# print(media)
		# start playing video
		# media.play()
		# stream = ffmpeg.input( first )
		# stream = ffmpeg.hflip(stream)
		# stream = ffmpeg.output(stream, "new_" + first )
		# ffmpeg.run(stream)
	def setSharpness( self , num ):
		self.sharpness = num
	def setHorizontal( self , param ):
		self.horizontalFlip = param
	def startProcessing(self):
		self.show_contents()
		self.textLabelInfo.setText( "Done Processing Videos" )
	def upload_button_click( self , widget ):
		# self.textLabel.setText("Please Select Videos!")
		self.textLabel.move(150,85)
		dialog = QFileDialog(
			widget,
			'Select directory of videos'
			# , self.homeDirectory
		)
		dialog.setFileMode(QFileDialog.DirectoryOnly)
		# print(dialog.exec_)
		# print(QDialog)
		# dialog.setSidebarUrls([QUrl.fromLocalFile(place)])
		if dialog.exec_() == QDialog.Accepted:
			self.selectedDirectory = dialog.selectedFiles()[ 0 ]
			self.textLabelInfo.setText( "Selected folder: " + self.selectedDirectory )
			# print(self.selectedDirectory)
			# widget._audio_file = dialog.selectedFiles()[0]

	def handleButtons( self , widget ):
	   self.textLabelInfo = QLabel()
	   self.textLabel = QLabel()
	   self.textLabel.setText( "StockVideos Video Processor" )
	   self.textLabel.setObjectName( 'heading' )
	   upload_button = QPushButton()
	   upload_button.setText( "Select Folder" )
	   start_button = QPushButton()
	   start_button.setText( "Start Processing" )
	   # upload_button.move( 64 , 32 )
	   start_button.clicked.connect(lambda: self.startProcessing())
	   upload_button.clicked.connect(lambda: self.upload_button_click(widget))


	   layout = QVBoxLayout(widget)
	   layout.addWidget(self.textLabel)
	   layout.addWidget(self.textLabelInfo)
	   # Horizontal Flip
	   flipBox = QHBoxLayout( widget )
	   flip_toggle = QRadioButton( 'Yes' )
	   flip_toggle.toggled.connect(lambda: self.setHorizontal('yes'))
	   flip_toggle.setChecked(True)
	   flip_toggle_1 = QRadioButton( 'No' )
	   flip_toggle.setObjectName( 'toggle' )
	   flip_toggle_1.setObjectName( 'toggle' )
	   flip_toggle_1.toggled.connect(lambda: self.setHorizontal('no'))
	   flip_title = QLabel( "Horizontal Flip" )
	   flipBox.addWidget(flip_title)
	   flipBox.addWidget(flip_toggle)
	   flipBox.addWidget(flip_toggle_1)
	   layout.addLayout(flipBox)
	   # Horizontal Flip
	   # Sharpness
	   sharpBox = QHBoxLayout( widget )
	   sharpSlider = QSlider(Qt.Horizontal)
	   sharpSlider.setMinimum(0)
	   sharpSlider.setMaximum(5)
	   sharpSlider.valueChanged.connect(lambda num: self.setSharpness(num))
	   sharpSlider.setTickPosition(QSlider.TicksBelow)
	   sharpSlider.setTickInterval(1)
	   sharp_title = QLabel( "Sharpness" )
	   sharpBox.addWidget(sharp_title)
	   sharpBox.addWidget(sharpSlider)
	   layout.addLayout(sharpBox)
	   # Sharpness


	   layout.addWidget(upload_button)
	   layout.addWidget(start_button)
	   # toggle_button.clicked.connect(toggle_changeColor)

	def start( self ):
	   app = QApplication( sys.argv )
	   app.setStyle( self.ui_theme )
	   widget = QWidget()
	   widget.setStyleSheet( open( 'style.css' ).read() )
	   self.handleButtons(widget)
	   # self.textLabel.move( 400 , 10 )
	   widget.setWindowIcon(QIcon('./favicon.png'))


		# layout->addWidget(button2);
		# layout->addWidget(button3);
		# layout->addWidget(button4);
		# layout->addWidget(button5);

	   widget.setGeometry(300,300,1000,500)
	   widget.setWindowTitle( windowTitle )
	   widget.show()
	   sys.exit(app.exec_())

def initialise():
	main = Main()
	main.start()

if __name__ == '__main__':
   initialise()