import sys
from os.path import expanduser
import os
from PyQt5.QtWidgets import QApplication , QWidget
from PyQt5.QtGui import QIcon
from pathlib import Path
from src.functions import upload_button_click , process_videos
from src.layout import handle_buttons
import shutil

windowTitle = "StockVideos Video Processor"

class Main:
	firstClickUpload = True
	durationStart = 0
	durationEnd = 0
	reversePlayBack = False
	reverseFfmpeg = False # reverse with ffmpeg, too much ram , preview not reversed
	filesList = []
	sharpness = 0
	saturation = 0
	speed=0
	horizontalFlip = False
	videoQuality = '4k'
	# selectedDirectory = "C:\\Users\\vmxit\\Desktop\\ama" # Testing
	selectedDirectory = ""
	processingDirectory = ""
	tempDirectory= ""
	cut = 0
	upScaling = False
	ui_theme = 'fusion'
	homeDirectory = expanduser( "~/Desktop" )
	def test( self ):
		print( '' )
	def show_contents( self ):
		self.filesList = [ f for f in os.listdir(self.selectedDirectory) if os.path.isfile(os.path.join(self.selectedDirectory,f)) ]
		self.processingDirectory = os.path.join( self.selectedDirectory , "processed" )
		self.tempDirectory = os.path.join( self.selectedDirectory , "temp" )
		Path( self.processingDirectory ).mkdir( exist_ok = True ) 
		Path( self.tempDirectory ).mkdir( exist_ok = True ) 
		x = 0
		for pathTmp in self.filesList:
			x = x + 1
			# print(pathTmp)
			path = os.path.join( self.selectedDirectory , pathTmp )
			newPath = os.path.join( self.processingDirectory , pathTmp )
			newPath = os.path.normpath( newPath )
			newTmpPath = os.path.join( self.tempDirectory , pathTmp )
			newTmpPath = os.path.normpath( newTmpPath )
			# print(path)
			path = os.path.normpath( path )
			self.widget.setWindowTitle( f'Processing {x} of {len( self.filesList )} videos' )
			# print(path,newPath)
			process_videos( self , path , newTmpPath , pathTmp , newPath )
	def setSpeed( self,num):
		self.speed = num
		self.speedState.setText( f"{0 if num < 1 else num/0.4}%" )
	def setUpscaling( self , param ):
		self.upScaling = param
	def setCut( self , num ):
		self.cut = num
		self.cutState.setText( f"{num}%" )
	def setSaturation( self,num):
		self.saturation = num
		self.saturState.setText( f"{num*10}%" )
	def setSharpness( self , num ):
		self.sharpness = num
		self.sharpState.setText( f"{num*10}%" )
	def setHorizontal( self , param ):
		self.horizontalFlip = param
	def setReverse( self , param ):
		self.reversePlayBack = param
	def startProcessing(self):
		self.show_contents()
		self.textLabelInfo.setText( "Done Processing Videos" )
		self.widget.setWindowTitle( "*Done Processing" )
		self.textLabelInfoSecondary.setText( "" )
		shutil.rmtree( self.tempDirectory , ignore_errors = True )

	def start( self ):
	   app = QApplication( sys.argv )
	   app.setStyle( self.ui_theme )
	   self.widget = QWidget()
	   self.widget.setStyleSheet( open( 'style.css' ).read() )
	   handle_buttons(self,self.widget)
	   # self.textLabel.move( 400 , 10 )
	   self.test()

		# layout->addWidget(button2);
		# layout->addWidget(button3);
		# layout->addWidget(button4);
		# layout->addWidget(button5);

	   self.widget.setWindowIcon(QIcon('./favicon.png'))
	   self.widget.setGeometry(300,300,1000,500)
	   self.widget.setWindowTitle( windowTitle )
	   self.widget.show()
	   # self.startProcessing() # Testing
	   sys.exit(app.exec_())

def initialise():
	main = Main()
	main.start()

if __name__ == '__main__':
   initialise()