import cv2
import time
from PyQt5.QtWidgets import QFileDialog , QDialog
from vidgear.gears import CamGear , WriteGear
from PIL import Image , ImageEnhance
import numpy as np
import os


def compressVideo( self , path , finalPath ):
	if ( self.reverseFfmpeg == True ):
		cmd = f"ffmpeg -i \"{path}\" \"{finalPath}\" -vf reverse -af areverse -hide_banner -loglevel error -y"
	else:
		cmd = f"ffmpeg -i \"{path}\" \"{finalPath}\" -hide_banner -loglevel error -y"
	# print(cmd)
	os.system( cmd )

def upScaleVideo( self , path , finalPath , width , height ):
	cmd = f"ffmpeg -i \"{path}\" -vf scale={width}:{height} \"{finalPath}\" -hide_banner -loglevel error -y"
	print(cmd)
	os.system( cmd )

def cutFrame( frame , num , height , width ):
	ratio = 60
	yStart = round( width * ( num / ratio ) )
	xStart = round( height * ( num / ratio ) )
	cropped = frame[ xStart:-xStart , yStart:-yStart ]
	dim = ( width, height )
	resized = cv2.resize( cropped, dim )
	return resized
def getSpeedState( self , totalFrames , i , frame_idx ):
	speedRatio = 40
	if ( i > totalFrames or frame_idx <= 0 ):
		return "break"
	if ( self.speed <= 0 ):
		return "proceed"
	if ( self.reversePlayBack == True ):
		# print(frame_idx , totalFrames * ( self.speed / speedRatio ))
		if ( frame_idx > totalFrames - ( totalFrames * ( self.speed / speedRatio ) ) ):
			return "continue"
		elif ( frame_idx < totalFrames * ( self.speed / speedRatio ) and frame_idx >= 0 ) :
			return "break"
	else:
		if ( i < totalFrames * ( self.speed / speedRatio ) ):
			return "continue"
		elif ( i > totalFrames - ( totalFrames * ( self.speed / speedRatio ) ) and i <= totalFrames ):
			return "break"
	return "proceed"
def setVideoQuality ( self , videoWidth , videoHeight ):
	if (
	( videoHeight > 4250 and videoWidth > 7500 ) or
	( videoHeight > 7500 and videoWidth > 4250 )
	):
		quality = '8k'
	elif (
	( videoHeight > 3000 and videoWidth > 1500 ) or
	( videoHeight > 1500 and videoWidth > 3000 )
	):
		quality = '4k'
	else:
		quality = '2k'
	self.videoQuality = quality
def process_videos( self , path , newPath , pathTmp , finalPath ):
	self.durationStart = time.perf_counter()
	progressText = "Currently processing: " + pathTmp;
	self.textLabelInfoSecondary.setText( progressText )
	capture = cv2.VideoCapture( path )
	totalFrames = int( capture.get( cv2.CAP_PROP_FRAME_COUNT ) )
	fps = capture.get( cv2.CAP_PROP_FPS )
	# stream = CamGear( source = path ).start()
	# fps = stream.framerate
	# stream.stop()
	frame_idx = totalFrames - 1
	i = 0
	length = totalFrames / fps
	writer = None
	# gpu_frame = cv2.cuda_GpuMat()
	start = time.perf_counter()
	# frame_idx = capture.get( cv2.CAP_PROP_FRAME_COUNT ) - 1
	# while capture.isOpened() and frame_idx >= 0:
	# 	capture.set(cv2.CAP_PROP_POS_FRAMES, frame_idx)
	# 	ret, frame = capture.read()
	# 	if ret is True:
	# 		frame_idx = frame_idx - 1
	# 		cv2.imshow('Frame in Reverse', frame)
	# 		if cv2.waitKey(30) & 0xFF == ord('q'):
	# 			break
	# 	else:
	# 		break
	# capture.release()
	# return
	while True:
	   # start = time.perf_counter()
	   frame_idx = frame_idx - 1
	   i = i + 1
	   speedState = getSpeedState( self , totalFrames , i , frame_idx )
	   if ( speedState == "continue" ):
	   	continue
	   elif ( speedState == "break" ):
	   	break
	   if ( self.reversePlayBack == True ):
	   	capture.set( cv2.CAP_PROP_POS_FRAMES , frame_idx )
	   ret , frame = capture.read()
	   # gpu_frame.upload(frame)
	   if frame is None:
	   	break
	   self.textLabelInfoSecondary.setText( f"{progressText} {round(( i / totalFrames ) * 100)}%" )
	   if frame is None:
	   	break
	   if not writer:
	   	height, width, channels = frame.shape
	   	videoIsVertical = height / width < 1
	   	setVideoQuality( self , width , height )
	   	fourcc = cv2.VideoWriter_fourcc(*'avc1')
	   	if ( self.upScaling == True and self.videoQuality == '2k' ):
	   		writer = cv2.VideoWriter(newPath, fourcc, fps, ( width*2,height*2))
	   	elif ( self.upScaling == True and self.videoQuality == '4k' ):
	   		# fourcc = cv2.VideoWriter_fourcc(*'MJPG')
	   		upScaleVideo( self , path , finalPath , width * 2 , height * 2 )
	   		return
	   	else:
	   		writer = cv2.VideoWriter(newPath, fourcc, fps, (width, height))
	   if self.horizontalFlip == True:
	   	frame = cv2.flip( frame , 1 )
	   if ( self.cut > 0 ):
	   	frame = cutFrame( frame , self.cut , height , width )
	   frames = Image.fromarray( frame )
	   frames = ImageEnhance.Color( frames ).enhance( ( 10 + self.saturation ) / 10 )
	   frames = ImageEnhance.Sharpness( frames ).enhance( self.sharpness )
	   finalFrame = np.array(frames)
	   if ( self.upScaling == True and self.videoQuality == '2k' ):
	   	finalFrame = resizeWithAspectRatio( self , finalFrame , width = width * 2 )
	   elif ( self.upScaling == True and self.videoQuality == '4k' ):
	   	finalFrame = resizeWithAspectRatio( self , finalFrame , width = width * 2 )
	   frameToShow = resizeWithAspectRatio(self,finalFrame,width=600)
	   cv2.imshow( "Playing" , frameToShow )
	   writer.write(finalFrame)
	   key = cv2.waitKey(30) & 0xFF
	   if key == ord("q"):
	   	break
	cv2.destroyAllWindows()
	capture.release()
	writer.release()
	compressVideo( self , newPath , finalPath )
	# print(time.perf_counter()-start)
	self.durationEnd = time.perf_counter() - self.durationStart


def upload_button_click( self , widget ):
	if ( self.firstClickUpload == True ):
		dialog = QFileDialog(
			widget,
			'Select directory of videos'
			, self.homeDirectory
		)
	else:
		dialog = QFileDialog(
			widget,
			'Select directory of videos'
		)
	self.firstClickUpload = False
	dialog.setFileMode(QFileDialog.DirectoryOnly)
	if dialog.exec_() == QDialog.Accepted:
		self.selectedDirectory = dialog.selectedFiles()[ 0 ]
		self.textLabelInfo.setText( "Selected folder: " + self.selectedDirectory )




def resizeWithAspectRatio(self,frame, width=None, height=None, inter=cv2.INTER_AREA):
    dim = None
    (h, w) = frame.shape[:2]

    if width is None and height is None:
        return frame
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(frame, dim, interpolation=inter)