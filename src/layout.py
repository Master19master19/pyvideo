from PyQt5.QtWidgets import QSlider , QVBoxLayout , QHBoxLayout , QLabel , QPushButton , QRadioButton , QButtonGroup
from PyQt5.QtCore import pyqtSlot,QUrl,Qt
from src.functions import upload_button_click , process_videos


def handle_buttons( self , widget ):
   self.textLabelInfo = QLabel()
   self.textLabelInfoSecondary = QLabel()
   self.textLabel = QLabel()
   self.textLabel.setText( "StockVideos Video Processor" )
   self.textLabel.setObjectName( 'heading' )
   upload_button = QPushButton()
   upload_button.setText( "Select Folder" )
   start_button = QPushButton()
   start_button.setText( "Start Processing" )
   # upload_button.move( 64 , 32 )
   start_button.clicked.connect(lambda: self.startProcessing())
   upload_button.clicked.connect(lambda: upload_button_click(self,widget))


   layout = QVBoxLayout(widget)
   layout.addWidget(self.textLabel)
   layout.addWidget(self.textLabelInfo)
   layout.addWidget(self.textLabelInfoSecondary)

   # Horizontal Flip
   flipBox = QHBoxLayout( widget )
   flip_toggle = QRadioButton( 'Yes' )
   flip_toggle.toggled.connect(lambda: self.setHorizontal(True))
   flip_toggle.setChecked(True)
   flip_toggle_1 = QRadioButton( 'No' )
   flip_toggle.setObjectName( 'toggle' )
   flip_toggle_1.setObjectName( 'toggle' )
   flip_toggle_1.toggled.connect(lambda: self.setHorizontal(False))
   flip_title = QLabel( "Horizontal Flip" )
   buttongroupHFlip = QButtonGroup(widget)
   buttongroupHFlip.addButton(flip_toggle)
   buttongroupHFlip.addButton(flip_toggle_1)
   flipBox.addWidget(flip_title)
   flipBox.addWidget(flip_toggle)
   flipBox.addWidget(flip_toggle_1)
   layout.addLayout(flipBox)
   # Horizontal Flip

   # Up Scaling
   scaleBox = QHBoxLayout( widget )
   scale_toggle = QRadioButton( 'Yes' )
   scale_toggle.toggled.connect(lambda: self.setUpscaling( True ) )
   scale_toggle_1 = QRadioButton( 'No' )
   scale_toggle.setObjectName( 'toggle' )
   scale_toggle_1.setObjectName( 'toggle' )
   scale_toggle_1.toggled.connect(lambda: self.setUpscaling( False ) )
   scale_toggle_1.setChecked( True )
   scale_title = QLabel( "Up Scaling" )
   buttongroup = QButtonGroup(widget)
   buttongroup.addButton(scale_toggle)
   buttongroup.addButton(scale_toggle_1)
   scaleBox.addWidget(scale_title)
   scaleBox.addWidget(scale_toggle)
   scaleBox.addWidget(scale_toggle_1)
   # scaleBox.addWidget(buttongroup)
   layout.addLayout( scaleBox )
   # Up Scaling

   
   # Reverse
   reverseBox = QHBoxLayout( widget )
   reverse_toggle = QRadioButton( 'Yes' )
   reverse_toggle.toggled.connect(lambda: self.setReverse(True))
   reverse_toggle_1 = QRadioButton( 'No' )
   reverse_toggle.setObjectName( 'toggle' )
   reverse_toggle_1.setObjectName( 'toggle' )
   reverse_toggle_1.toggled.connect(lambda: self.setReverse(False))
   reverse_toggle_1.setChecked( True )
   reverse_title = QLabel( "Reverse" )
   buttongroupHreverse = QButtonGroup(widget)
   buttongroupHreverse.addButton(reverse_toggle)
   buttongroupHreverse.addButton(reverse_toggle_1)
   reverseBox.addWidget(reverse_title)
   reverseBox.addWidget(reverse_toggle)
   reverseBox.addWidget(reverse_toggle_1)
   layout.addLayout(reverseBox)
   # Reverse



   # Cut
   cutBox = QHBoxLayout( widget )
   cutSlider = QSlider( Qt.Horizontal )
   cutSlider.setMinimum( 0 )
   cutSlider.setMaximum( 3 )
   cutSlider.valueChanged.connect(lambda num: self.setCut(num))
   cutSlider.setTickPosition( QSlider.TicksBelow )
   cutSlider.setTickInterval( 1 )
   cut_title = QLabel( "Positioning" )
   cut_title.setObjectName( 'toggle_title' )
   cutBox.addWidget(cut_title)
   cutBox.addWidget(cutSlider)
   self.cutState = QLabel( f"{self.cut}%" )
   self.cutState.setObjectName( 'toggle_info' )
   cutBox.addWidget( self.cutState )
   layout.addLayout(cutBox)
   # Cut


   # Speed
   speedBox = QHBoxLayout( widget )
   speedSlider = QSlider( Qt.Horizontal )
   speedSlider.setMinimum( 0 )
   speedSlider.setMaximum( 10 )
   speedSlider.valueChanged.connect(lambda num: self.setSpeed(num))
   speedSlider.setTickPosition( QSlider.TicksBelow )
   speedSlider.setTickInterval( 1 )
   speed_title = QLabel( "Cut Video" )
   speed_title.setObjectName( 'toggle_title' )
   speedBox.addWidget(speed_title)
   speedBox.addWidget(speedSlider)
   self.speedState = QLabel( f"{self.speed}%" )
   self.speedState.setObjectName( 'toggle_info' )
   speedBox.addWidget( self.speedState )
   layout.addLayout(speedBox)
   # Speed



   # Sharpness
   sharpBox = QHBoxLayout( widget )
   sharpSlider = QSlider(Qt.Horizontal)
   sharpSlider.setMinimum(0)
   sharpSlider.setMaximum(5)
   sharpSlider.valueChanged.connect(lambda num: self.setSharpness(num))
   sharpSlider.setTickPosition(QSlider.TicksBelow)
   sharpSlider.setTickInterval(1)
   sharp_title = QLabel( "Sharpness" )
   sharpBox.addWidget(sharp_title)
   sharp_title.setObjectName( 'toggle_title' )
   sharpBox.addWidget(sharpSlider)
   self.sharpState = QLabel( f"{self.sharpness}%" )
   self.sharpState.setObjectName( 'toggle_info' )
   sharpBox.addWidget( self.sharpState )
   layout.addLayout(sharpBox)
   # Sharpness
   
   # Saturation
   saturBox = QHBoxLayout( widget )
   saturSlider = QSlider( Qt.Horizontal )
   saturSlider.setMinimum(-5)
   saturSlider.setMaximum(10)
   saturSlider.valueChanged.connect(lambda num: self.setSaturation(num))
   saturSlider.setTickPosition(QSlider.TicksBelow)
   saturSlider.setTickInterval(1)
   satur_title = QLabel( "Saturation" )
   satur_title.setObjectName( 'toggle_title' )
   saturBox.addWidget( satur_title )
   saturBox.addWidget( saturSlider )
   self.saturState = QLabel( f"{self.saturation}%" )
   self.saturState.setObjectName( 'toggle_info' )
   saturBox.addWidget( self.saturState )
   layout.addLayout(saturBox)
   # Saturation

   

   layout.addWidget(upload_button)
   layout.addWidget(start_button)
   # toggle_button.clicked.connect(toggle_changeColor)
